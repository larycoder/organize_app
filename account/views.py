from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth


def register(request):

    if request.method == 'POST':
        first_name = request.POST['first_name']
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if User.objects.filter(username=username).exists():
            messages.info(request, 'Username taken!')
            return redirect('register')
        elif password1 == password2:
            user = User.objects.create_user(username=username, password=password1, first_name=first_name)
            user.save()
            return redirect('log_in')
        else:
            messages.info(request, 'Passwords does not match!')
            return redirect('register')
    else:
        return render(request, 'register.html')


def log_in(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)
        if user:
            auth.login(request, user)
            return redirect('/')
        else:
            messages.info(request, "Username or password does not match")
            return redirect('log_in')
    else:
        return render(request, 'log_in.html')


def logout(request):

    auth.logout(request)
    return redirect('/')