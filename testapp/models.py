from django.db import models


class TestModel(models.Model):
    name = models.CharField(max_length=100)
    disc = models.CharField(max_length=250)
    test_bool = models.BooleanField(default=False)