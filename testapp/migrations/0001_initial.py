# Generated by Django 3.2 on 2021-04-11 15:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TestModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('disc', models.CharField(max_length=250)),
                ('test_bool', models.BooleanField(default=False)),
            ],
        ),
    ]
